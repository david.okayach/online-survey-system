import {createTheme} from '@mui/material/styles';

export const theme = createTheme({
    palette: {
        gold: {
            main: '#ffe87b',
        },
        secondary: {
            main: '#ff1e58',
        },
        white: {
            main: '#ffffff',
        },
        black: {
            main: '#000000',
        },
        text: {
            primary: '#000000',
            secondary: '#FFD700',
        },
    },
});