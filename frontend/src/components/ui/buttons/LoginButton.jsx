import {Box, Menu, Stack, TextField, Tooltip} from "@mui/material";
import PropTypes from "prop-types";
import {useState} from "react";
import {SecondaryButton} from "@components/ui/buttons/SecondaryButton";
import {PrimaryButton} from "@components/ui/buttons/PrimaryButton.jsx";
import {useLoginMutation} from "@services/store/api/authApi.jsx";
import useTopSnackbar from "@components/ui/snackbars/TopSnackbar.jsx";

export const LoginButton = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [loginForm, setLoginForm] = useState(null);
    const [login] = useLoginMutation();
    const snackbar = useTopSnackbar();

    const handleLogin = async () => {
        try {
            const {error, data} = await login({email, password});
            let messages = [];

            if (error) {
                if (error.data.errors) {
                    messages = error.data.errors.map(err => `${err.field}: ${err.message}`);
                } else {
                    messages = [error.data.message];
                }
            } else {
                messages = [data.message];
            }

            messages.forEach(message => snackbar(message, error ? "error" : "success"));
            console.log(error || data);
        } catch (error) {
            snackbar("An unexpected error occurred", "error");
            console.log(error)
        }
    };

    const handleOpenLoginMenu = (event) => {
        setLoginForm(event.currentTarget);
    };
    const handleCloseLoginMenu = () => {
        setLoginForm(null);
    };

    return (<Box sx={{flexGrow: 0}}>
        <Tooltip title="Open login form">
            <SecondaryButton onClick={handleOpenLoginMenu}
            >Login</SecondaryButton>
        </Tooltip>
        <Menu
            sx={{mt: '45px'}}
            id="menu-appbar"
            anchorEl={loginForm}
            anchorOrigin={{
                vertical: 'top', horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
                vertical: 'top', horizontal: 'right',
            }}
            open={Boolean(loginForm)}
            onClose={handleCloseLoginMenu}
        >
            <Stack spacing={2} padding={2}>
                <TextField variant={"standard"} onChange={e => setEmail(e.target.value)} label="Email"
                           sx={{color: 'gold.main'}}/>
                <TextField variant={"standard"} onChange={e => setPassword(e.target.value)} label="Password"
                           type="password"/>
                <PrimaryButton onClick={handleLogin}>
                    Login
                </PrimaryButton>
            </Stack>
        </Menu>
    </Box>)
}
