package com.example.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class OnlineSurveySystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineSurveySystemApplication.class, args);
    }
}
